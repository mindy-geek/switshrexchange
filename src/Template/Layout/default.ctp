<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="GeekSolutions">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css('bootstrap/css/bootstrap.css') ?>
        <?= $this->Html->css('font-awesome/css/font-awesome.min.css') ?>

        <?= $this->Html->css('slick/slicknav.css') ?>
        <?= $this->Html->css('slick/slick.css') ?>
        <?= $this->Html->css('slick/slick-theme.css') ?>

        <?= $this->Html->css('style') ?>

        <?= $this->Html->css('products-list') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
    </head>
    <body>
        <!-- Navigation -->
        <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top <?= ($this->request->params['controller'] == 'Index') ? 'index' : '' ?>">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand page-scroll" href="<?= $this->Url->Build(['_name' => 'home']) ?>">
                        <?= $this->Html->Image('switshr-logo-pink.png') ?>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <?php if (!empty($logged_user) && $logged_user['id']){ ?>
                        <?= $this->element('inline-menu') ?>
                    <?php }else{ ?>
                        <?= $this->element('offline-menu') ?>
                    <?php } ?>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <?= $this->Flash->render() ?>

        <?= $this->fetch('content') ?>

        <div class="pre-footer">
            <div class="site-logo">
                <?= $this->Html->Image('switshr-logo.png') ?>
            </div>
            <label><?= __('Tel : ') ?></label><span>(+000) 00 000 00</span>
            <span> / </span>
            <label><?= __('Fax : ') ?></label><span>(+000) 00 000 00</span>
            <br/>
            <label><?= __('Email : ') ?></label><span>contact@switshr.com</span>
            <div>
                <ul class="social-buttons">
                    <li>
                        <a href="#">
                            <?= $this->Html->Image('icon-facebook.png') ?>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <?= $this->Html->Image('icon-google-plus.png') ?>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <?= $this->Html->Image('icon-twitter.png') ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 text-left footer-block">
                        <span>SWITSHR 2017</span>
                    </div>
                    <div class="col-xs-6 text-right footer-block">
                        <a href="#"><?= __('About') ?></a>
                        <span>-</span>
                        <a href="#"><?= __('Help') ?></a>
                        <span>-</span>
                        <a href="#"><?= __('Contact') ?></a>
                        <span>-</span>
                        <a href="#"><?= __('Terms and Policy') ?></a>
                    </div>
                </div>
            </div>

            <?= $this->Html->script('jquery/jquery.min') ?>
            <?= $this->Html->script('bootstrap/js/bootstrap.min') ?>

            <?= $this->Html->script('slicknav.js') ?>
            <?= $this->Html->script('slick/slick.min.js') ?>

            <?= $this->Html->script('agency.js') ?>

            <?= $this->fetch('script') ?>
        </footer>
        <?= $this->element('user-forms-modals') ?>
    </body>
</html>
