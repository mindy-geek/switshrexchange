<div id="signin-page">
    <section class="auth-form">
        <h1 class="modal-title text-center"><?= __('Sign in') ?></h1>
        <div class="social-btns text-center">
            <a href="#" class="btn facebook-btn btn-rounded">
                <i class="fa fa-facebook"></i>
                <?= __('Sign in with facebook') ?>
            </a>
            <a href="#" class="btn google-btn btn-rounded">
                <i class="fa fa-google-plus"></i>
                <?= __('Sign in with google') ?>
            </a>
        </div>
        <div class="block-separator">
            <span><?= __('Or') ?></span>
        </div>
        <form method="POST" action="<?= $this->Url->Build(['_name' => 'signin']) ?>">
            <div class="input-group col-xs-12">
                <?= $this->Form->input('email',['type' => 'email' , 'class' => 'form-control' , 'placeholder' => __('Email address') , 'label' => false]) ?>
            </div>
            <div class="input-group col-xs-12">
                <?= $this->Form->input('password',['type' => 'password' , 'class' => 'form-control' , 'placeholder' => __('Password') , 'label' => false]) ?>
            </div>
            <p>
                <a href="#"><small><?= __('Reset password') ?></small></a>
            </p>
            <div class="text-center">
                <button type="submit" class="btn btn-rounded btn-pink fill-width"><?= __('Sign in') ?></button>
            </div>
            <p>
                <small><?= __('Not a Switshr member yet ?') ?> <a href="<?= $this->Url->Build(['_name' => 'signup']) ?>"><?= __('Sign up') ?></a></small>
            </p>
        </form>
    </section>
</div>