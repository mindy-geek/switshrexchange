<!-- Header -->
<header>
    <div class="container-fluid">
        <div class="intro-text">
            <?= $this->Html->Image('switshr-photo.png') ?>
            <p><?= __('Together,we are connected by the things we own the things we do and the things we love.') ?></p>
            <div class="text-center">
                <a href="#" class="btn btn-white btn-rounded text-uppercase"><?= __('All ads') ?></a>
                <a href="#" class="btn btn-pink btn-rounded text-uppercase"><?= __('Post an ad') ?></a>
            </div>
        </div>
    </div>
</header>

<section class="white-bg">
    <div class="container">
        <div class="row section-header">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><?= __('Why join Switshr ?') ?></h2>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4 service-block">
                <?= $this->Html->Image('icon-trade-goods.png', ['class' => 'service-icon']) ?>
                <h4 class="service-heading"><?= __('Trade goods') ?></h4>
                <p class="text-muted"><?= __('Exchange unwanted goods, benefit from their second lives!') ?></p>
            </div>
            <div class="col-md-4 service-block">
                <?= $this->Html->Image('icon-access-knowledge.png', ['class' => 'service-icon']) ?>
                <h4 class="service-heading"><?= __('Access Knowledge') ?></h4>
                <p class="text-muted">Exchange experience and know-how, get in touch with the right people!</p>
            </div>
            <div class="col-md-4 service-block">
                <?= $this->Html->Image('icon-grow-passions.png', ['class' => 'service-icon']) ?>
                <h4 class="service-heading"><?= __('Grow passions') ?></h4>
                <p class="text-muted">Exchange tips and ideas of the things you love, enjoy growing your hobbies!</p>
            </div>
        </div>
    </div>
</section>

<section class="grey-section">
    <div class="container">
        <div class="row section-header">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><?= __('How it works ?') ?></h2>
                <p>
                    <?= __('Switshr allows to create mutually beneficial relationships') ?>
                </p>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4 service-block">
                <?= $this->Html->Image('step-1-icon.png', ['class' => 'service-icon']) ?>
                <p class="text-muted"><?= __('Define what you need or what you offer. Post an ad!') ?></p>
            </div>
            <div class="col-md-4 service-block">
                <?= $this->Html->Image('step-2-icon.png', ['class' => 'service-icon']) ?>
                <p class="text-muted"><?= __('Find suitable Switshr ads around you. Send a request!') ?></p>
            </div>
            <div class="col-md-4 service-block">
                <?= $this->Html->Image('step-3-icon.png', ['class' => 'service-icon']) ?>
                <p class="text-muted"><?= __('Pick your best match. Swap it!') ?></p>
            </div>
        </div>
    </div>
</section>

<section class="white-bg">
    <div class="container">
        <div class="row section-header">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><?= __('Advertise on item now') ?></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <p>Sed haec nihil sane ad rem; Illud non continuo, ut aeque incontentae.</p>
                <p>Nescio quo modo praetervolavit oratio. Non quam nostram quidem, inquit Pomponius iocans.</p>
                <div>
                    <a href="#" class="btn btn-pink btn-rounded text-uppercase"><?= __('Post an ad') ?></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="switshr-slogan-block">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center section-header">
                <h2 class="section-heading"><?= __('Switshr generation') ?></h2>
                <p><?= __('A word of exchange empowered by human connections') ?></p>
            </div>
            <div class="col-xs-12 text-center">
                <div class="slick-block">
                    <div class="slick-item">
                        <?= $this->Html->Image('icon-reviews.png') ?>
                        <p><?= __('You may not need 500 friends but only those who matter') ?></p>
                    </div>
                    <div class="slick-item">
                        <?= $this->Html->Image('icon-reviews.png') ?>
                        <p><?= __('You may not need 500 friends but only those who matter') ?></p>
                    </div>
                    <div class="slick-item">
                        <?= $this->Html->Image('icon-reviews.png') ?>
                        <p><?= __('You may not need 500 friends but only those who matter') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="white-bg">
    <div class="container">
        <div class="row section-header">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><?= __('Our categories') ?></h2>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 category-item">
                        <a href="#">
                            <?= $this->Html->Image('category-home.jpg') ?>
                            <label><?= __('Home') ?></label>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 category-item">
                        <a href="#">
                            <?= $this->Html->Image('category-hightech.jpg') ?>
                            <label><?= __('High tech') ?></label>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 category-item">
                        <a href="#">
                            <?= $this->Html->Image('category-clothes.jpg') ?>
                            <label><?= __('Clothes and accessories') ?></label>
                        </a>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4 category-item">
                        <a href="#">
                            <?= $this->Html->Image('category-sport.jpg') ?>
                            <label><?= __('Sports accessories') ?></label>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 category-item">
                        <a href="#">
                            <?= $this->Html->Image('category-eduction.jpg') ?>
                            <label><?= __('Education and culture') ?></label>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 category-item">
                        <a href="#">
                            <?= $this->Html->Image('category-special-items.jpg') ?>
                            <label><?= __('Specialitems') ?></label>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>