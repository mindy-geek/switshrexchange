<div id="find-ads-container" class="container">
    <div class="breadcrumb">
        <?php $this->Html->addCrumb(__('All ads'),['controller' => 'Ads' , 'action' => 'index']) ?>
        <?= $this->Html->getCrumbs(' > ', [
            'text' => '<i class="fa fa-home"></i>',
            'url' => ['controller' => 'Pages', 'action' => 'display', 'home'],
            'escape' => false
        ]); ?>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-3">
            <?= $this->element('ads-filter-bar') ?>
        </div>
        <div class="col-xs-12 col-sm-9">

            <?= $this->element('find-ads-pagination-bar') ?>

            <div class="ads-list-block grid">
                <!-- Ad Item block -->
                <div class="ad-block-item">
                    <div class="picture">
                        <a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><?= $this->Html->Image('laptop.jpg') ?></a>
                    </div>
                    <div class="captcha">
                        <div class="item-title"><a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><strong>Lorem ipsum dolor sit amet</strong></a></div>
                        <div><i class="fa fa-map-marker"></i> Manouba, Manouba Tunisia</div>
                        <div class="condition-block"><i class="fa fa-star"></i> <?= __('Condition') ?> : Brand new. Perfect inside and out</div>
                        <div><i class="fa fa-user"></i> <?= __('By') ?> : Nihed</div>
                        <div><i class="fa fa-calendar"></i> <?= __('On') ?> 22/07/2017</div>
                        <div class="item-price"><span><?= __('Value') ?> : </span> <span class="price">50</span></div>
                    </div>
                    <div class="action-btn">
                        <a href="#" class="btn btn-rounded btn-pink"><?= __('Ask for exchange') ?></a>
                    </div>
                </div>

                <!-- Ad Item block -->
                <div class="ad-block-item">
                    <div class="picture">
                        <a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><?= $this->Html->Image('iphone.jpg') ?></a>
                    </div>
                    <div class="captcha">
                        <div class="item-title"><a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><strong>Lorem ipsum dolor sit amet</strong></a></div>
                        <div><i class="fa fa-map-marker"></i> Manouba, Manouba Tunisia</div>
                        <div class="condition-block"><i class="fa fa-star"></i> <?= __('Condition') ?> : Brand new. Perfect inside and out</div>
                        <div><i class="fa fa-user"></i> <?= __('By') ?> : Nihed</div>
                        <div><i class="fa fa-calendar"></i> <?= __('On') ?> 22/07/2017</div>
                        <div class="item-price"><span><?= __('Value') ?> : </span> <span class="price">350</span></div>
                    </div>
                    <div class="action-btn">
                        <a href="#" class="btn btn-rounded btn-pink"><?= __('Ask for exchange') ?></a>
                    </div>
                </div>

                <!-- Ad Item block -->
                <div class="ad-block-item">
                    <div class="picture">
                        <a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><?= $this->Html->Image('playstation.jpg') ?></a>
                    </div>
                    <div class="captcha">
                        <div class="item-title"><a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><strong>Lorem ipsum dolor sit amet</strong></a></div>
                        <div><i class="fa fa-map-marker"></i> Manouba, Manouba Tunisia</div>
                        <div class="condition-block"><i class="fa fa-star"></i> <?= __('Condition') ?> : Brand new. Perfect inside and out</div>
                        <div><i class="fa fa-user"></i> <?= __('By') ?> : Nihed</div>
                        <div><i class="fa fa-calendar"></i> <?= __('On') ?> 22/07/2017</div>
                        <div class="item-price"><span><?= __('Value') ?> : </span> <span class="price">299</span></div>
                    </div>
                    <div class="action-btn">
                        <a href="#" class="btn btn-rounded btn-pink"><?= __('Ask for exchange') ?></a>
                    </div>
                </div>

                <!-- Ad Item block -->
                <div class="ad-block-item">
                    <div class="picture">
                        <a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><?= $this->Html->Image('faux-bijoux.jpg') ?></a>
                    </div>
                    <div class="captcha">
                        <div class="item-title"><a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><strong>Lorem ipsum dolor sit amet</strong></a></div>
                        <div><i class="fa fa-map-marker"></i> Manouba, Manouba Tunisia</div>
                        <div class="condition-block"><i class="fa fa-star"></i> <?= __('Condition') ?> : Brand new. Perfect inside and out</div>
                        <div><i class="fa fa-user"></i> <?= __('By') ?> : Nihed</div>
                        <div><i class="fa fa-calendar"></i> <?= __('On') ?> 22/07/2017</div>
                        <div class="item-price"><span><?= __('Value') ?> : </span> <span class="price">20</span></div>
                    </div>
                    <div class="action-btn">
                        <a href="#" class="btn btn-rounded btn-pink"><?= __('Ask for exchange') ?></a>
                    </div>
                </div>

                <!-- Ad Item block -->
                <div class="ad-block-item">
                    <div class="picture">
                        <a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><?= $this->Html->Image('laptop.jpg') ?></a>
                    </div>
                    <div class="captcha">
                        <div class="item-title"><a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><strong>Lorem ipsum dolor sit amet</strong></a></div>
                        <div><i class="fa fa-map-marker"></i> Manouba, Manouba Tunisia</div>
                        <div class="condition-block"><i class="fa fa-star"></i> <?= __('Condition') ?> : Brand new. Perfect inside and out</div>
                        <div><i class="fa fa-user"></i> <?= __('By') ?> : Nihed</div>
                        <div><i class="fa fa-calendar"></i> <?= __('On') ?> 22/07/2017</div>
                        <div class="item-price"><span><?= __('Value') ?> : </span> <span class="price">50</span></div>
                    </div>
                    <div class="action-btn">
                        <a href="#" class="btn btn-rounded btn-pink"><?= __('Ask for exchange') ?></a>
                    </div>
                </div>

                <!-- Ad Item block -->
                <div class="ad-block-item">
                    <div class="picture">
                        <a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><?= $this->Html->Image('iphone.jpg') ?></a>
                    </div>
                    <div class="captcha">
                        <div class="item-title"><a href="<?= $this->Url->Build(['_name' => 'view-ad']) ?>"><strong>Lorem ipsum dolor sit amet</strong></a></div>
                        <div><i class="fa fa-map-marker"></i> Manouba, Manouba Tunisia</div>
                        <div class="condition-block"><i class="fa fa-star"></i> <?= __('Condition') ?> : Brand new. Perfect inside and out</div>
                        <div><i class="fa fa-user"></i> <?= __('By') ?> : Nihed</div>
                        <div><i class="fa fa-calendar"></i> <?= __('On') ?> 22/07/2017</div>
                        <div class="item-price"><span><?= __('Value') ?> : </span> <span class="price">350</span></div>
                    </div>
                    <div class="action-btn">
                        <a href="#" class="btn btn-rounded btn-pink"><?= __('Ask for exchange') ?></a>
                    </div>
                </div>
            </div>
            <div class="pagination fill-width">
                <table class="fill-width">
                    <tr>
                        <td>
                            <a href="#" class="pagination-element"><span><i class="fa fa-angle-left"></i></span></a>
                            <a href="#" class="pagination-element"><span>1</span></a>
                            <a href="#" class="pagination-element"><span>2</span></a>
                            <a href="#" class="pagination-element"><span>3</span></a>
                            <a href="#" class="pagination-element"><span><i class="fa fa-angle-right"></i></span></a>
                        </td>
                        <td class="text-right">
                            ( 25 items found showing 1-6)
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->start('script') ?>
<script>

    if($('.ads-list-block').hasClass('list')){
        generateResponsiveList('list');
    }else{
        generateResponsiveList('grid');
    }

    $('#grid-mod-btn').click(function () {
        if(!$('.ads-list-block').hasClass('grid')){
            generateResponsiveList('grid');
            $(this).addClass('active');
            $('#list-mod-btn').removeClass('active');
        }
    });

    $('#list-mod-btn').click(function () {
        if(!$('.ads-list-block').hasClass('list')) {
            generateResponsiveList('list');
            $(this).addClass('active');
            $('#grid-mod-btn').removeClass('active');
        }
    });


    function generateResponsiveList(mod) {
        switch (mod) {
            case 'list':
                if($('.ads-list-block').hasClass('grid')) {
                    $('.ads-list-block').removeClass('grid');
                    $('.ads-list-block').removeClass('row');
                    $('.ads-list-block').find('.ad-block-item').removeClass('col-xs-12 col-sm-6 col-md-4');
                    $('.ads-list-block').find('.ad-block-item').each(function () {
                        var block_content = $(this).find('.ad-block-wrapper').html();
                        $(this).html('');
                        $(this).append(block_content);
                    });
                }
                $('.ads-list-block').addClass('list');
                break;
            case 'grid':
                if($('.ads-list-block').hasClass('list')) {
                    $('.ads-list-block').removeClass('list');
                }
                $('.ads-list-block').addClass('grid').addClass('row');
                $('.ads-list-block').find('.ad-block-item').addClass('col-xs-12 col-sm-6 col-md-4');
                $('.ads-list-block').find('.ad-block-item').each(function () {
                    var block_content = $(this).html();
                    $(this).html('');
                    $(this).append('<div class="ad-block-wrapper"></div>');
                    $(this).find('.ad-block-wrapper').append(block_content);
                });
                break;
        }
    }

</script>
<?php $this->end() ?>