<?= $this->Html->css('jquery.bxslider/jquery.bxslider.min') ?>
<?= $this->Html->css('product') ?>

<article id="view-ad-container" class="container">
    <div class="breadcrumb">
        <?php $this->Html->addCrumb(__('All ads')) ?>
        <?php $this->Html->addCrumb('Felix 1 Seater Blue Fabric Sofa',['controller' => 'Ads' , 'action' => 'view']) ?>
        <?= $this->Html->getCrumbs(' > ', [
            'text' => '<i class="fa fa-home"></i> '.__('Home'),
            'url' => ['controller' => 'Pages', 'action' => 'display', 'home'],
            'escape' => false
        ]); ?>
    </div>

    <div class="row">
        <!-- Product picture block -->
        <div class="col-xs-12 col-sm-8 col-md-8">
            <div class="product-details-block view-picture-block">
                <ul class="main-image-slider">
                    <li><?= $this->Html->Image('canape.jpg') ?></li>
                    <li><?= $this->Html->Image('test/001.jpg') ?></li>
                    <li><?= $this->Html->Image('test/002.jpg') ?></li>
                    <li><?= $this->Html->Image('test/003.jpg') ?></li>
                    <li><?= $this->Html->Image('test/004.jpg') ?></li>
                    <li><?= $this->Html->Image('test/005.jpg') ?></li>
                    <li><?= $this->Html->Image('test/006.jpg') ?></li>
                </ul>

                <div id="pictures-pager">
                    <a data-slide-index="0" href="">
                        <?= $this->Html->Image('canape.jpg') ?>
                    </a>
                    <a data-slide-index="1" href="">
                        <?= $this->Html->Image('test/001.jpg') ?>
                    </a>
                    <a data-slide-index="2" href="">
                        <?= $this->Html->Image('test/002.jpg') ?>
                    </a>
                    <a data-slide-index="3" href="">
                        <?= $this->Html->Image('test/003.jpg') ?>
                    </a>
                    <a data-slide-index="4" href="">
                        <?= $this->Html->Image('test/004.jpg') ?>
                    </a>
                    <a data-slide-index="5" href="">
                        <?= $this->Html->Image('test/005.jpg') ?>
                    </a>
                    <a data-slide-index="6" href="">
                        <?= $this->Html->Image('test/006.jpg') ?>
                    </a>
                </div>
            </div>
        </div>

        <!-- Right Side Bar -->
        <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="product-details-block text-center">
                <h1>Felix 1 Seater Blue Fabric Sofa</h1>
                <small><i class="fa fa-clock-o"></i> 20 April 10:08</small>
                <hr/>
                <span><?= __('Value') ?> : </span> <span class="price">50</span>
            </div>
            <div class="product-details-block">
                <h2><?= __('Details') ?></h2>
                <p><i class="fa fa-map-marker"></i> Manouba,Manouba,Tunisia</p>
                <p><i class="fa fa-star"></i> <?= __('Condition') ?> : Brand new, perfect inside and out</p>
            </div>
            <div class="product-details-block text-center">
                <div class="user-picture">
                    <?= $this->Html->Image('jhon-doe.jpg') ?>
                    <p>Jhon Doe</p>
                </div>
                <hr/>
                <a href="#" class="btn btn-pink btn-rounded"><?= __('Contact') ?></a>
                <a href="#" class="btn btn-pink btn-rounded"><?= __('Ask for exchange') ?></a>
            </div>
            <div class="product-details-block text-center">
                <ul class="social-buttons">
                    <li>
                        <a href="#" class="facebook"></a>
                    </li>
                    <li>
                        <a href="#" class="twitter"></a>
                    </li>
                    <li>
                        <a href="#" class="google"></a>
                    </li>
                </ul>
                <hr/>
                <a href="#"> <i class="fa fa-heart"></i> <?= __('Add to wishlist') ?></a>
            </div>
        </div>

        <!-- Description block -->
        <div class="col-xs-12">
            <div class="product-details-block">
                <h2><?= __('Description') ?></h2>
                <hr/>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet.</p>
                <p>Cupiditates non Epicuri divisione finiebat, sed sua satietate. Frater et T. Nos vero, inquit ille; Si quidem, inquit, tollerem, sed relinquo. Quod cum accidisset ut alter alterum necopinato videremus, surrexit statim. Eorum enim est haec querela, qui sibi cari sunt seseque diligunt. Qui ita affectus, beatum esse numquam probabis.</p>
            </div>
        </div>

    </div>

</article>

<?php $this->start('script'); ?>
    <?= $this->Html->Script('jquery.bxslider/jquery.bxslider.min') ?>
    <script type="text/javascript">
        $('.main-image-slider').bxSlider({
            pagerCustom: '#pictures-pager',
            controls:false,
            touchEnabled:false,
        });

        $('#pictures-pager').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode:true,
            focusOnSelect: true,
            arrows: true,
        });
    </script>
<?php $this->end(); ?>
