<div id="find-ads-pagination-bar" class="box-shadow">
    <div class="pagination-item">
        <span><?= __('Filter by') ?></span>
        <?= $this->Form->Select('filter-by',['Low to Hight' , 'Hight to Low'],['class' => 'form-control']) ?>
    </div>
    <div class="pagination-item text-right">
        <a href="#" id="grid-mod-btn"><i class="fa fa-th"></i></a>
        <a href="#" id="list-mod-btn"><i class="fa fa-list"></i></a>
    </div>
</div>