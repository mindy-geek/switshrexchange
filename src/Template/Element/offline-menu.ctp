<ul class="top-menu nav navbar-nav navbar-right">
    <li class="hidden">
        <a href="#page-top"></a>
    </li>
    <li>
        <a class="page-scroll" href="<?= $this->Url->Build(['_name' => 'home']) ?>"><?= __('Home') ?></a>
    </li>
    <li>
        <a class="page-scroll" href="<?= $this->Url->Build(['_name' => 'find-ads']) ?>"><?= __('Find ads') ?></a>
    </li>
    <li>
        <a class="page-scroll" href="#"><?= __('Post ad') ?></a>
    </li>
    <?php if($this->request->controller !== 'Users' || $this->request->action !== 'add'){ ?>
        <li>
            <a class="page-scroll" data-toggle="modal" data-target="#signup-modal" href="#team"><?= __('Sign up') ?></a>
        </li>
    <?php } ?>
    <?php if($this->request->action !== 'login'){ ?>
        <li>
            <a class="page-scroll" data-toggle="modal" data-target="#signin-modal" href="#team"><?= __('Sign in') ?></a>
        </li>
    <?php } ?>
    <li class="langages">
        <a class="page-scroll" href="#contact"><?= __('En') ?></a>
    </li>
</ul>