<div id="ads-filter-bar" class="filters-block box-shadow">
    <!-- Types of ads block -->
    <div class="filter-item">
        <h2 class="filter-title"><?= __('Types of ads') ?></h2>
        <div class="filter-elements">
            <ul>
                <li>
                    <label for="all" class="filter-element">
                        <?= $this->Form->checkbox('types[]',['hiddenField' => false , 'id' => 'all']); ?>
                        <?= __('All') ?>
                    </label>
                </li>
                <li>
                    <label for="offer" class="filter-element">
                        <?= $this->Form->checkbox('types[]',['hiddenField' => false , 'id' => 'offer']); ?>
                        <?= __('Offer') ?>
                    </label>
                </li>
                <li>
                    <label for="request" class="filter-element">
                        <?= $this->Form->checkbox('types[]',['hiddenField' => false , 'id' => 'request']); ?>
                        <?= __('Request') ?>
                    </label>
                </li>
            </ul>
        </div>
    </div>
    <!-- Categories of ads block -->
    <div class="filter-item">
        <h2 class="filter-title"><?= __('Categories') ?></h2>
        <div class="filter-elements">
            <ul>
                <li>
                    <span class="filter-element">
                        <a href="#"><?= __('All') ?></a>
                    </span>
                </li>
                <li>
                    <span class="filter-element">
                        <a href="#"><?= __('Home') ?></a>
                    </span>
                </li>
                <li>
                    <span class="filter-element">
                        <a href="#"><?= __('Hightech') ?></a>
                    </span>
                </li>
                <li>
                    <span class="filter-element">
                        <a href="#"><?= __('Clothes & accessories') ?></a>
                    </span>
                </li>
                <li>
                    <span class="filter-element">
                        <a href="#"><?= __('Education & culture') ?></a>
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <!-- Ads location block -->
    <div class="filter-item">
        <h2 class="filter-title"><?= __('City') ?></h2>
        <div class="filter-elements">
            <?= $this->Form->Input('city',['class' => 'form-control' , 'placeholder' => __('Type city name') ,'label' => false]) ?>
        </div>
    </div>
    <!-- Ads location block -->
    <div class="filter-item">
        <div class="filter-elements">
            <?= $this->Form->input(__('Search'),['type'=> 'submit' , 'class' => 'btn btn-rounded btn-pink']) ?>
        </div>
    </div>
</div>