<ul class="nav navbar-nav navbar-right">
    <li class="hidden">
        <a href="#page-top"></a>
    </li>
    <li>
        <a class="page-scroll" href="<?= $this->Url->Build(['_name' => 'home']) ?>"><?= __('Home') ?></a>
    </li>
    <li>
        <a class="page-scroll" href="<?= $this->Url->Build(['_name' => 'find-ads']) ?>">
            <?= __('Find ads') ?>
        </a>
    </li>
    <li>
        <a class="page-scroll" href="#"><?= __('Post ad') ?></a>
    </li>
    <li>
        <a class="page-scroll" data-toggle="dropdown" href="#"><?= __('My account') ?></a>
        <ul class="dropdown-menu" aria-labelledby="dLabel">
            <li>
                <a href="#"><i class="fa fa-user-o"></i> <?= __('My profile') ?></a>
            </li>
            <li><a href="#"><i class="fa fa-tasks"></i> <?= __('My activities') ?></a></li>
            <li><a href="#"><i class="fa fa-bell-o"></i> <?= __('Notifications') ?> &nbsp;<span class="badge red">7</span></a></li>
            <li><a href="#"><i class="fa fa-envelope-open-o"></i> <?= __('My messages') ?> &nbsp;<span class="badge red">3</span></a></li>
            <li><a href="#"><i class="fa fa-heart-o"></i> <?= __('My favorites') ?></a></li>
            <li><a href="#"><i class="fa fa-plus-square-o"></i> <?= __('Invite my friend') ?></a></li>
            <li><a href="#"><i class="fa fa-cog"></i> <?= __('Setting') ?></a></li>
            <li><a href="#"><i class="fa fa-handshake-o"></i> <?= __('Contact') ?></a></li>
            <li><a href="<?= $this->Url->Build(['_name' => 'logout']) ?>"><i class="fa fa-power-off"></i> <?= __('Logout') ?></a></li>
        </ul>
    </li>
    <li>
        <a class="page-scroll" href="#contact"><?= __('En') ?></a>
    </li>
</ul>