<?php

namespace App\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Event\Event;
use Cake\Network\Request;
use Cake\Network\Response;



class CookieAuthenticate extends BaseAuthenticate
{
    /**
     * Authenticates the identity contained in a request.
     *
     * @param \Cake\Network\Request $request The request that contains login information.
     * @param \Cake\Network\Response $response Unused response object.
     * @return array|bool Array of user info on success, false on falure.
     */
    public function authenticate(Request $request, Response $response)
    {
        $fields = $this->_config['fields'];
        
        $userToken = $this->_registry->Cookie->read($this->_config['keyToken']);
        if (empty($userToken)) {
            return false;
        }

        if (empty($userToken[$fields['username']]) || empty($userToken[$fields['password']])) {
            return false;
        }
        return $this->_findUser(
            $userToken[$fields['username']], 
            $userToken[$fields['password']]
        );

    }
    
    /**
     * Returns a list of all events that this authenticate class will listen to.
     * 
     * @return array List of events this class listens to.
     */
    public function implementedEvents()
    {
        return ['Auth.logout' => 'logout'];
    }
    
    /**
     * Delete cookies when an user logout.
     *
     * @param \Cake\Event\Event $event The logout Event.
     * @param array $user The user about to be logged out.
     *
     * @return void
     */
    public function logout(Event $event, array $user)
    {
        $this->_registry->Cookie->delete($this->_config['keyToken']);
    }
}